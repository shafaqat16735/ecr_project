﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECR_Project.Models;

namespace ECR_Project.Areas.Admin.Controllers
{
    public class tblPoliceOfficerRecordsController : Controller
    {
        private Entities db = new Entities();

        // GET: Admin/tblPoliceOfficerRecords
        public ActionResult Index()
        {
            var tblPoliceOfficerRecords = db.tblPoliceOfficerRecords.Include(t => t.tblGender).Include(t => t.tblPoliceStation);
            return View(tblPoliceOfficerRecords.ToList());
        }

        // GET: Admin/tblPoliceOfficerRecords/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblPoliceOfficerRecord tblPoliceOfficerRecord = db.tblPoliceOfficerRecords.Find(id);
            if (tblPoliceOfficerRecord == null)
            {
                return HttpNotFound();
            }
            return View(tblPoliceOfficerRecord);
        }

        // GET: Admin/tblPoliceOfficerRecords/Create
        public ActionResult Create()
        {
            ViewBag.GenderID = new SelectList(db.tblGenders, "ID", "Gender");
            ViewBag.DutyStationID = new SelectList(db.tblPoliceStations, "ID", "PSName");
            return View();
        }

        // POST: Admin/tblPoliceOfficerRecords/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,FatherName,DateOfBirth,GenderID,Email,Password,CNIC,Adress,City,Post,BeltNo,DutyStationID,DateOfCharge,SiteArea,ContactNo")] tblPoliceOfficerRecord tblPoliceOfficerRecord)
        {
            if (ModelState.IsValid)
            {
                db.tblPoliceOfficerRecords.Add(tblPoliceOfficerRecord);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GenderID = new SelectList(db.tblGenders, "ID", "Gender", tblPoliceOfficerRecord.GenderID);
            ViewBag.DutyStationID = new SelectList(db.tblPoliceStations, "ID", "PSName", tblPoliceOfficerRecord.DutyStationID);
            return View(tblPoliceOfficerRecord);
        }

        // GET: Admin/tblPoliceOfficerRecords/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblPoliceOfficerRecord tblPoliceOfficerRecord = db.tblPoliceOfficerRecords.Find(id);
            if (tblPoliceOfficerRecord == null)
            {
                return HttpNotFound();
            }
            ViewBag.GenderID = new SelectList(db.tblGenders, "ID", "Gender", tblPoliceOfficerRecord.GenderID);
            ViewBag.DutyStationID = new SelectList(db.tblPoliceStations, "ID", "PSName", tblPoliceOfficerRecord.DutyStationID);
            return View(tblPoliceOfficerRecord);
        }

        // POST: Admin/tblPoliceOfficerRecords/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,FatherName,DateOfBirth,GenderID,Email,Password,CNIC,Adress,City,Post,BeltNo,DutyStationID,DateOfCharge,SiteArea,ContactNo")] tblPoliceOfficerRecord tblPoliceOfficerRecord)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblPoliceOfficerRecord).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GenderID = new SelectList(db.tblGenders, "ID", "Gender", tblPoliceOfficerRecord.GenderID);
            ViewBag.DutyStationID = new SelectList(db.tblPoliceStations, "ID", "PSName", tblPoliceOfficerRecord.DutyStationID);
            return View(tblPoliceOfficerRecord);
        }

        // GET: Admin/tblPoliceOfficerRecords/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblPoliceOfficerRecord tblPoliceOfficerRecord = db.tblPoliceOfficerRecords.Find(id);
            if (tblPoliceOfficerRecord == null)
            {
                return HttpNotFound();
            }
            return View(tblPoliceOfficerRecord);
        }

        // POST: Admin/tblPoliceOfficerRecords/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblPoliceOfficerRecord tblPoliceOfficerRecord = db.tblPoliceOfficerRecords.Find(id);
            db.tblPoliceOfficerRecords.Remove(tblPoliceOfficerRecord);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
